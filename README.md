# urosjarc.github.io

#### Requirements

A full TeX distribution is assumed.  [Various distributions for different operating systems (Windows, Mac, \*nix) are available](http://tex.stackexchange.com/q/55437) but TeX Live is recommended.
You can [install TeX from upstream](http://tex.stackexchange.com/q/1092) (recommended; most up-to-date) or use `sudo apt-get install texlive-full` if you really want that.  (It's generally a few years behind.)

#### Usage

At a command prompt, run

```bash
$ xelatex {your-cv}.tex
```

This should result in the creation of ``{your-cv}.pdf``

#### My fast bio

I got first in contact with programming when I was 9 years old in school class where we were programming "Turtle graphics". Then my knowledge was updated when I was at engineering school at age 16 where I was in contact with CNC machine programming, and PIC microcontroller technologies. I had a lot of fun as I remember. When I mature in my knowledge I entered "Faculty of Mathematics and Physics" as a physics student. I learned about C, Python, Matlab, Mathematica programming languages, and about wide area of mathematics and physics which put me in the best position to become good programmer. And little did I know, linear algebra class gave me knowledge to become potentially expert in machine learning, artificial intelligence and deep artificial neural network, computer based physics simulations. When I discover that now are the golden age for programmers, I started my path as backend developer on Node.js. I curently have 2 years programming exp. as server developer, but focusing my best effort to become full fledged artificial intelligence expert in tensorflow.

